
from django.shortcuts import render
from PD.models import Vacancies

def vacancies_view(request):
    vacancies = Vacancies.objects.all()
    context = {
            'vacancies': vacancies,
    }
    return  render(request,'vacancies.html',context)

def vacancies_detail_view(request, id_vacancies):
    vacancy = Vacancies.objects.get(id = id_vacancies)
    context = {
        'vacancy':vacancy
    }
    return render(request, 'vacancies_detail.html', context)