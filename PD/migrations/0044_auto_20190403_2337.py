# Generated by Django 2.1.7 on 2019-04-03 20:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0043_vacancies_datetime'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vacancies',
            old_name='datetime',
            new_name='date',
        ),
    ]
