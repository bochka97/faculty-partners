# Generated by Django 2.1.7 on 2019-04-05 13:31

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0044_auto_20190403_2337'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='date',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AddField(
            model_name='partners',
            name='date',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AddField(
            model_name='practice',
            name='date',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
