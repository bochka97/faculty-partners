# Generated by Django 2.1.7 on 2019-03-18 05:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0017_auto_20190318_0802'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='company',
            field=models.CharField(max_length=128, null=True, verbose_name='Компания'),
        ),
    ]
