# Generated by Django 2.1.7 on 2019-04-03 20:36

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0042_auto_20190402_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancies',
            name='datetime',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
