# Generated by Django 2.1.7 on 2019-04-07 19:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0057_auto_20190407_2243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='name',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='ФИО'),
        ),
    ]
