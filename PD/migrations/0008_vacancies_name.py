# Generated by Django 2.1.7 on 2019-03-14 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0007_partners_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancies',
            name='name',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
