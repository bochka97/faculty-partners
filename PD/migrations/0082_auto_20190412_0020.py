# Generated by Django 2.1.7 on 2019-04-11 21:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PD', '0081_auto_20190412_0018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='priority',
            field=models.IntegerField(blank=True, choices=[('Работает в компании', 'Работает в компании'), ('Не работает в компании', 'Не работает в компании')], default='Работает в компании', null=True, verbose_name='Статус'),
        ),
    ]
