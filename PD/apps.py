# interstore/apps.py
from django.apps import AppConfig

class PDAppConfig(AppConfig):
    name = "PD" # Здесь указываем исходное имя приложения
    verbose_name = "Партнеры факультета" # А здесь, имя которое необходимо отобразить в админке