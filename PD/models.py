from django.db import models
import datetime
from djchoices import ChoiceItem, DjangoChoices
from django.urls import reverse


class Partners(models.Model):

    profile = models.ForeignKey('Profile',
                             on_delete=models.CASCADE, verbose_name='Профиль', null=True)
    position = models.CharField(max_length=128, null=True, blank =True, verbose_name='Должность')
    technology = models.ManyToManyField('Technology', null=True, blank =True, verbose_name='Технологии')
    company = models.ForeignKey('Company',
                                on_delete=models.CASCADE, null=True, verbose_name='Компания')
    email = models.EmailField(verbose_name='Корпоративная почта', null=True, unique=True)
    phone = models.CharField(max_length=12, null=True, blank =True, verbose_name='Телефон')
    site = models.CharField(max_length=128, null=True, blank =True, verbose_name='Сайт')

    class PriorityType(DjangoChoices):
        work = ChoiceItem('Работает в компании', 'Работает в компании')
        unwork = ChoiceItem('Не работает в компании', 'Не работает в компании')
    priority = models.CharField(max_length=50, choices=PriorityType.choices, default=PriorityType.unwork, verbose_name='Статус', null=True, blank =True)
    date = models.DateField(default=datetime.date.today, verbose_name='Дата')

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'

    def __str__(self):
     return self.profile.name
    #def __str__(self):
    #    return (self.name)

class Company(models.Model):
    name = models.CharField(max_length=128, verbose_name='Компания',unique=True)
    educational_program = models.ManyToManyField('Directions', verbose_name='Специальности', blank = True, null=True)
    technology = models.ManyToManyField('Technology', verbose_name='Технологии', blank = True, null=True)
    site = models.CharField(max_length=128, null=True, blank=True, verbose_name='Сайт компании')
    description = models.CharField(max_length=512, null=True, verbose_name='Описание компании', blank=True)
    collaboration_category = models.ManyToManyField('Collaboration', null=True, blank =True, verbose_name='Категории сотрудничества')

    date = models.DateField(default=datetime.date.today, verbose_name='Дата')

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'

    def __str__(self):
        return (self.name)

class Collaboration(models.Model):
    category = models.CharField(max_length=50, verbose_name='Категория сотрудничества')

    class Meta:
        verbose_name = 'Категория сотрудничества'
        verbose_name_plural = 'Категории сотрудничества'

    def __str__(self):
        return (self.category)


class Directions(models.Model):
    direction = models.CharField(max_length=50, verbose_name='Направление')

    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

    def __str__(self):
        return (self.direction)

class VacanciesManager(models.Manager):
    def all(self, *args, **kwargs):
        return super(VacanciesManager, self).get_queryset().filter(status='Актуальная')

class Vacancies(models.Model):
    name = models.CharField(max_length=128, verbose_name='Название')
    experience = models.CharField(max_length=128, null=True, verbose_name='Опыт', blank =True)
    employment = models.CharField(max_length=128, null=True, verbose_name='Занятость', blank =True)
    requirements= models.TextField(max_length=512, null=True, verbose_name='Требования', blank =True)
    company = models.ForeignKey('Company',
        on_delete=models.CASCADE, verbose_name='Компания', null=True, blank =True)

    class ActualType(DjangoChoices):
        arch = ChoiceItem('Архивная', 'Архивная')
        moder = ChoiceItem('На модерации', 'На модерации')
        actu = ChoiceItem('Актуальная', 'Актуальная')

    status = models.CharField(max_length=50, choices=ActualType.choices, default=ActualType.moder, verbose_name='Статус',)
    site = models.CharField(max_length=128, null=True, verbose_name='Сайт', blank =True)
    duty = models.TextField(max_length=512, null=True, verbose_name='Обязанности', blank=True)
    direction = models.ManyToManyField('Directions',
    null = True, verbose_name='Специальности', blank =True)

    description = models.TextField(max_length = 512, null = True, verbose_name='Описание', blank =True)

    date = models.DateField(default=datetime.date.today, verbose_name='Дата')

    objects = VacanciesManager()

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'

    def __str__(self):
        return (self.name)

    def get_absolute_url(self):
        return reverse('vacancies_detail', kwargs = {'id_vacancies':self.id})


class Technology(models.Model):
    name = models.CharField(max_length=50, verbose_name='Технологии')

    class Meta:
        verbose_name = 'Технология'
        verbose_name_plural = 'Технологии'
    def __str__(self):
        return (self.name)




class Profile(models.Model):
    name = models.CharField(max_length=128, verbose_name='ФИО')
    email2 = models.EmailField(verbose_name='Личная почта', null=True,unique=True)
    phone = models.CharField(max_length=12, null=True, blank=True, verbose_name='Телефон')
    description = models.CharField(max_length=512, null=True, verbose_name='Описание', blank=True)
    date = models.DateField(default=datetime.date.today, verbose_name='Дата')

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


    def __str__(self):
        return (self.name)


















