from  django import forms
from .models import *

class PartnersForm(forms.ModelForm):

    class Meta:
        model = Partners
        exclude = [""]

    class VacanciesForm(forms.ModelForm):
            class Meta:
                model = Vacancies
                exclude = [""]

     class DirectionsForm(forms.ModelForm):
            class Meta:
                model = Directions
                exclude = [""]

