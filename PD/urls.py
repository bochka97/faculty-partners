
from django.urls import path, include

from  PD.views import vacancies_view, vacancies_detail_view


urlpatterns = [
    path('vacancies/<int:id_vacancies>/', vacancies_detail_view, name = 'vacancies_detail'),
    path('',vacancies_view, name='vacancies'),

]