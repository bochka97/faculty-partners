from django.contrib import  admin
from .models import *
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
#from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter



class CompanyResource(resources.ModelResource):
    name = fields.Field(attribute='name', column_name='Название')
    educational_program = fields.Field(attribute='educational_program', column_name='Направление',widget = ManyToManyWidget(Directions, field='direction'))
    technology = fields.Field(attribute='technology', column_name='Технологии',widget = ManyToManyWidget(Technology, field='name'))
    site = fields.Field(attribute='site', column_name='Сайт компании')
    description = fields.Field(attribute='description', column_name='Описание компании')
    collaboration_category = fields.Field(
    attribute='collaboration_category', column_name='Категории сотрудничества',widget = ManyToManyWidget(Collaboration, field='category'))
    date = fields.Field(attribute='date', column_name='Дата', )
    class Meta:
        model = Company
        import_id_fields = ('id',)

class PartnersInline(admin.TabularInline):
        model = Partners

class CompanyAdmin(ImportExportModelAdmin, admin.ModelAdmin):

    list_display = ['name', 'site', 'description',]
    search_fields = ['name','educational_program__direction','collaboration_category__category', ]
    filter_horizontal = ('educational_program','technology','collaboration_category')
    date_hierarchy = 'date'
    inlines = [
        PartnersInline,
    ]


    resource_class = CompanyResource



class PartnersResource(resources.ModelResource):
    profile = fields.Field(attribute='profile', column_name='ФИО',widget = ForeignKeyWidget(Profile, 'name'))

    position = fields.Field(attribute='position', column_name='Должность')
    technology = fields.Field(attribute='technology', column_name='Технологии',
                              widget=ManyToManyWidget(Technology, field='name'))
    company = fields.Field(attribute='company', column_name='Компания',widget = ForeignKeyWidget(Company, 'name'))
    email = fields.Field(attribute='email', column_name='Корпоративная почта')

    phone = fields.Field(attribute='phone', column_name='Телефон')
    site = fields.Field(attribute='site', column_name='Сайт')
    priority = fields.Field(attribute='priority', column_name='Статус',)
    date = fields.Field(attribute='date', column_name='Дата', )
    class Meta:
        model = Partners
        import_id_fields = ['email']



class PartnersAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['profile','email','position','company','phone','site', 'priority',]
    list_filter = ['profile', 'company', 'priority']
    search_fields = ['profile','email','position','company','phone','site','priority',]
    filter_horizontal = ('technology',)
    date_hierarchy = 'date'
    resource_class = PartnersResource



class VacanciesResource(resources.ModelResource):
    name = fields.Field(attribute='name', column_name='Название')
    status = fields.Field(attribute='status', column_name='Статус', )
    direction = fields.Field(attribute='direction', column_name='Специальности',
                                       widget=ManyToManyWidget(Directions, field='direction'))
    company = fields.Field(attribute='company', column_name='Компания', widget=ForeignKeyWidget(Company, 'name'))
    description = fields.Field(attribute='description', column_name='Описание')
    experience = fields.Field(attribute='experience', column_name='Опыт')
    site = fields.Field(attribute='site', column_name='Сайт')
    employment = fields.Field(attribute='employment', column_name='Занятость')
    requirements = fields.Field(attribute='requirements', column_name='Требования')
    duty = fields.Field(attribute='duty', column_name='Обязанности')
    date = fields.Field(attribute='date', column_name='Дата', )

    class Meta:
        model = Vacancies
        import_id_fields = ['id']


class VacanciesAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['name','company','status','experience', 'employment', 'requirements', 'description', ]
    filter_horizontal = ('direction',)
    list_filter = ['name', 'company', 'status']
    date_hierarchy = 'date'
    search_fields = ['name','description','direction__direction', 'status']
    resource_class = VacanciesResource

class ProfileResource(resources.ModelResource):
    name = fields.Field(attribute='name', column_name='ФИО')
    email2 = fields.Field(attribute='email2', column_name='Личная почта')

    phone = fields.Field(attribute='phone', column_name='Телефон')
    description = fields.Field(attribute='description', column_name='Описание')
    date = fields.Field(attribute='date', column_name='Дата', )

    class Meta:
        model = Profile
        import_id_fields = ['email2']





class ProfileAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['name', 'email2', 'phone', 'description']
    list_filter = ['name']
    search_fields = ['name', 'email2', 'phone', 'description']
    date_hierarchy = 'date'
    inlines = [
        PartnersInline,
    ]
    resource_class = ProfileResource




admin.site.register(Directions)
admin.site.register(Technology)
admin.site.register(Collaboration)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Partners, PartnersAdmin)
admin.site.register(Vacancies, VacanciesAdmin)
admin.site.register(Profile,ProfileAdmin)




